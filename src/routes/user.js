const router = require('express-promise-router')();
const { checkJWTSign } = require("../middlewares/jwtCheck.middleware") //проверка на токен
const { user } = require('../controllers');

router.route('/:id').put(user.update);
router.route('/:email').get(checkJWTSign, user.getEmail);

module.exports = router;