import crudStore from './generic'

//Передаем хранилищу путь и название, по которому будем взаимодействовать с таблицей анкет
export default {
	...crudStore({
		url: 'user',
		name: 'user',
	}),
}