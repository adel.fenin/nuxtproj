import GenericService from '@/services/generic.service'

const crudStore = ({ name, url, keyName }) => {
	const genericService = new GenericService ({
		name,
		url
	})
	return {
		state: () => ({
			item: {},
			itemError: null
		}),
		//объявляем actions для каждого соответствующего метода внутри generic.services
		actions: {
			async update({ commit }, { id, payload }) {
				try {
					const item = await genericService.update(id, payload)
					commit('updateItemSuccess', item)
				} catch (err) {
					commit('updateItemFail', {
						errType: `${name} UPDATE`,
						err
					})
				}
			},
			async fetchEmail({ commit }, email) {
				try {
					const item = await genericService.fetchEmail(email)
					commit('fetchItemNameSuccess', item)
				} catch (err) {
					commit('fetchItemNameFail', {
						errType: `${name} FETCHONE`,
						err
					})
				}
			},
		},
		mutations: {
			updateItemSuccess(state, item) {
				state.item = item
			},
			updateItemFail(state, err) {
				state.itemError = err
			},
			fetchItemNameSuccess(state, item) {
				state.item = item
			},
			fetchItemNameFail(state, err) {
				state.itemError = err
			},
		},
		getters: {
			item: (state) => state.item,
			itemError: (state) => state.itemError,
		}
	}
}

export default crudStore