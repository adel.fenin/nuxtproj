require('dotenv').config() 
const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");
const http = require("http");
const mongoose = require("mongoose");
//Импорт всх роутов
const { routes } = require("./src/routes");

//Подключение к бд
mongoose.connect(
	process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
})
.then(() => console.log("MongoDB database connection success..."))
.catch((err) => console.log(err))

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//Объявляю роуты
routes.forEach((item) => {
	app.use(`/api/v1/${item}`, require(`./src/routes/${item}`));
});

const PORT = 3000
http.createServer({}, app).listen(PORT);
console.log(`Server has been started on port: ${PORT}`);