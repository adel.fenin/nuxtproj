let client

export function setClient(newClient) {
	client = newClient
}

const reqMethods = [
	'get',
	'delete',
	'post',
	'put',
	'head',
	'options',
	'request',
]

let service = {}

reqMethods.forEach((method) => {
	service[method] = function () {
		if (!client) throw new Error("Axios not installed")
		return client[method].apply(null, arguments)
	}
})

export default service