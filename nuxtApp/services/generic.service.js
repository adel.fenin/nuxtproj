import axios from './request.service'

//Класс отвечающий за все запросы
export default class GenericService {
	constructor({ url, name }) {
		this.url = url
		this.name = name
	}
	async update(id, payload) {
		try {
			const { data } = await axios.put(`${this.url}/${id}`, payload)
			return data
		} catch(err) {
			throw {
				err,
				error: true,
				message: `${this.name} on UPDATE something wrong`,
			}
		}
	}
	async fetchEmail(email) {
		try {
			const { data } = await axios.get(`${this.url}/${email}`)
			return data
		} catch(err) {
			throw {
				err,
				error: true,
				message: `${this.name} on FETCHONE something wrong`,
			}
		}
	}
}