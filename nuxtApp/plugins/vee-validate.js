import { extend } from 'vee-validate'
import { required, alpha, min, max, regex, email } from 'vee-validate/dist/rules'

extend('required', {
	...required,
	message: 'Поле обязательно для заполнения'
})

extend('email', {
	...email,
	message: 'Введите корректный email'
})

extend('min', {
	...min,
	message: 'Нужно ввести более 7 символов'
})

extend('max', {
	...max,
	message: 'Максимальная длина ФИО 40 символов'
})

extend('regex', {
	...regex,
	message: 'Только русские буквы'
})