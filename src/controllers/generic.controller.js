const boom = require('boom');

//Описываю контроллер моделей
const genericCrud = (model) => ({
	async update({ params: { id }, body }, res) {
		try {
			const item = await model.findByIdAndUpdate(id, body, { new: true });
			return res.status(200).send(item);
		} catch(err) {
			return res.status(400).send(boom.boomify(err));
		}
	},
	async getEmail({ params: { email } }, res) {
		try {
			const item = await model.findOne({email: email});
			return res.status(200).send(item);
		} catch(err) {
			return res.status(400).send(boom.boomify(err));
		}
	},
});

module.exports = genericCrud;