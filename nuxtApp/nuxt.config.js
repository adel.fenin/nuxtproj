export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'nuxtApp',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
	'~/plugins/axiosport.js',
	'~/plugins/vee-validate.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
	'@nuxtjs/axios',
	'@nuxtjs/auth-next',
  ],
  
  axios: {
	  //Базовая ссылка, с которой приложение будет контактировать
	  baseURL: process.env.NUXT_APP_BASE_URL,
  },
  router: {
	middleware: ['auth']  
  },
  auth: {
	  strategies: {
		  local: {
			  scheme: 'refresh',
			  token: {
				property: 'accessToken',
				maxAge: 1800,
				//type: 'Bearer'
			  },
			  refreshToken: {
				property: 'refreshToken',
				data: 'refreshToken',
				maxAge: '60 * 60 * 24 *30',
			  },
			  endpoints: {
				  user: false,
				  refresh: {
					url: '/auth/refresh',
					method: 'post',
				  },
				  login: {
					url: '/auth/login',
					method: 'post',
					propertyName: 'accessToken'
				  },
				  logout: {
					  url: '/auth/logout',
					  method: 'post'
				  }
			  }
		  },
	  }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
	  transpile: ['vee-validate/dist/rules'],
  }
}
