//Описываю схему в БД
const {
	model, 
	Schema, 
	Schema: { 
		Types: { ObjectId },
	},
} = require('mongoose');

const schema = new Schema({
	userName: {
		type: String,
		default: "",
	},
	email: {
		type: String,
		default: ""
	},
	password: {
		type: String,
		default: "",
	},
});

module.exports = model('User', schema);