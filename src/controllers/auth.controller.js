require('dotenv').config()
const jwt = require('jsonwebtoken')
const { omit } = require('ramda')
const { User, Token } = require('../models')

const ACCESS_TOKEN_LIFE = '60m'

module.exports = {
	async logout({ body: { refreshToken } }, res) {

		const foundToken = await Token.findOne({ token: refreshToken })
		
		if(!foundToken) {
			return res.status(403).send({
				message: 'Пользователь не авторизован'
			})
		}
		
		await Token.findByIdAndDelete(foundToken._id)
		
		return res.status(200).send({
			message: 'Пользователь успешно разлогинен'
		})

	},
	//реализация рефреш токена
	async refreshToken({ body: { refreshToken } }, res) {
		//Проверяем есть ли токен в запросе на сервер
		if(!refreshToken) {
			return res.status(403).send({
				message: 'Действие запрещено'
			})
		}
		//ищем токен в БД
		const currentToken = await Token.findOne({ token: refreshToken })
		//если не находим токен - ошибка
		if(!currentToken) {
			return res.status(403).send({
				message: 'Действие запрещено'
			})
		}
		
		jwt.verify(refreshToken, process.env.JWT_SECRET_REFRESH, (err, user) => {
			if(err) {
				return res.status(403).send({
					message: 'Действие запрещено'
				})
			}

			const accessToken = jwt.sign({
				userId: user._id,
				email: user.email,
			}, process.env.JWT_SECRET, {
				expiresIn: ACCESS_TOKEN_LIFE
			})
			
			return res.status(200).send({
				accessToken,
				email: user.email
			})
		})
	},
	//реализация авторизации пользователя
	async login({ body: { email, password } }, res) {
		try {
			const foundUser = await User.findOne({ email })
			
			if (!foundUser) {
				return res.status(403).send({
					message: 'Извините, но логин или пароль не подходит',
					err
				})
			}
			
			// Шифруем пароль из БД, сравниваем
			const isPasswordCorrect = foundUser.password === password
			
			if (!isPasswordCorrect) {
				return res.status(403).send({
					message: 'Извините, но логин или пароль не подходит',
					err
				})
			}
			
			const accessToken = jwt.sign({
				userId: foundUser._id,
				email: foundUser.email,
			}, process.env.JWT_SECRET, {
				expiresIn: ACCESS_TOKEN_LIFE
			})
			
			const refreshToken = jwt.sign({
				userId: foundUser._id,
				email: foundUser.email,
			}, process.env.JWT_SECRET_REFRESH)
			
			const foundToken = await Token.findOne({
				user: foundUser._id
			})
			
			if(foundToken){
				await Token.findByIdAndUpdate(foundToken._id, { token: refreshToken })
				return res.status(200).send({
					accessToken,
					refreshToken,
					email: foundUser.email
				})
			}
			
			const item = new Token({ token: refreshToken, user: foundUser._id });
			await item.save();
			
			return res.status(200).send({
				accessToken,
				refreshToken,
				email: foundUser.email
			})

		} catch(err) {
			return res.status(403).send({
				message: 'Извините, но логин или пароль не подходит',
				err
			})
		}
	},
	//реализация регистрации пользователя
	async signUp({ body: { email, password, userName } }, res) {
		try {
			const foundUser = await User.findOne({ email })
			if (foundUser) {
				return res.status(403).send({
					message: 'Данный логин занят',
					err
				})
			}
			const createdUser = await new User({ userName ,email, password })
			await createdUser.save();
			
			
			return res.status(200).send({
				message: 'Пользователь создан'
			})
			
		} catch(err) {
			return res.status(403).send({
				message: 'Извините, но логин или пароль не подходит',
				err
			})
		}
	}
}